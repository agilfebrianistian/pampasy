//
//  FeedCell.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 10/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import UIKit
import SwipeCellKit

class FeedCell: SwipeTableViewCell {
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        likeButton.layer.cornerRadius = likeButton.frame.size.height/4
        likeButton.layer.masksToBounds = true
        likeButton.layer.borderColor = UIColor(red: 208/255, green: 2/255, blue: 27/255, alpha: 1).cgColor
        likeButton.layer.borderWidth = 0.5
        likeButton.backgroundColor = UIColor(red: 255/255, green: 235/255, blue: 238/255, alpha: 1)
        
        commentButton.layer.cornerRadius = commentButton.frame.size.height/4
        commentButton.layer.masksToBounds = true
        commentButton.layer.borderColor = UIColor.lightGray.cgColor
        commentButton.layer.borderWidth = 0.5
        
        likeButton.backgroundColor = UIColor(red: 255/255, green: 235/255, blue: 238/255, alpha: 1)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
