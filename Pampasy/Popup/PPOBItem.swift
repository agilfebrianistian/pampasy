//
//  PPOBItem.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 19/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import Foundation
import UIKit

class PPOBItem: UIView {
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var title: UILabel!
}

