//
//  PostPopup.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 28/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import UIKit
import PopupController

class PostPopup : UIViewController, PopupContentViewController {
    
    class func instance() -> PostPopup {
        let storyboard = UIStoryboard(name: "PostPopup", bundle: nil)
        return storyboard.instantiateInitialViewController() as! PostPopup
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width,height: UIScreen.main.bounds.height)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
