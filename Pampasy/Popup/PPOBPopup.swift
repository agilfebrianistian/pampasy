//
//  PPOBPopup.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 19/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import UIKit
import PopupController
import iCarousel

class PPOBPopup: UIViewController, PopupContentViewController {
    
    @IBOutlet weak var PPOBItem: iCarousel!
    @IBOutlet weak var promoItem: iCarousel!
    
    class func instance() -> PPOBPopup {
        let storyboard = UIStoryboard(name: "PPOBPopup", bundle: nil)
        return storyboard.instantiateInitialViewController() as! PPOBPopup
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width,height: UIScreen.main.bounds.height*0.6)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PPOBItem.type = .linear
        promoItem.type = .linear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PPOBPopup : iCarouselDelegate, iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        let count = 5
        let placeholder = 3
        
        return count >= 3 ? count - placeholder : count
    }
    
    func numberOfPlaceholders(in carousel: iCarousel) -> Int {
        let count = 5
        let placeholder = 3
        
        return count >= 3 ? count - placeholder : 0
    }
    
    func carousel(_ carousel: iCarousel, placeholderViewAt index: Int, reusing view: UIView?) -> UIView {
        let index = index == 0 ? -1 : carousel.numberOfItems
        return self.carousel(carousel, viewForItemAt:index, reusing: view)
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        if carousel.tag == 0 {
            let view = Bundle.main.loadNibNamed("PPOBItem", owner: self, options: nil)![0] as! PPOBItem
            
            view.imageContainer.layer.cornerRadius = view.imageContainer.frame.size.height/4
            view.imageContainer.layer.masksToBounds = true
            view.imageContainer.layer.borderColor = UIColor.lightGray.cgColor
            view.imageContainer.layer.borderWidth = 0.5
            
            view.iconImage.image = #imageLiteral(resourceName: "pulsa")
            view.title.text = "Pulsa"
            
            return view
        }
        else {
            
            let itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 240, height: 120))            
            itemView.image = #imageLiteral(resourceName: "samplePromo")
            
            return itemView
        }

    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        print("du du du")
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            if carousel.tag == 0{
                return value * 1.3
            }
            else {
                return value * 1
            }
        }
        return value
    }
}
