//
//  FontManager.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 28/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import Foundation
import UIKit

public enum FontStyle: String {
    case black = "Nunito-Black"
    case italicBlack = "Nunito-BlackItalic"
    case bold = "Nunito-Bold"
    case italicBold = "Nunito-BoldItalic"
    case extraBold = "Nunito-ExtraBold"
    case italicExtraBold = "Nunito-ExtraBoldItalic"
    case extraLight = "Nunito-ExtraLight"
    case italicExtraLight = "Nunito-ExtraLightItalic"
    case italic = "Nunito-Italic"
    case light = "Nunito-Light"
    case italicLight = "Nunito-LightItalic"
    case regular = "Nunito-Regular"
    case semiBold = "Nunito-SemiBold"
    case italicSemiBold = "Nunito-SemiBoldItalic"
    }

extension UIFont {
    class func regularFont(size: CGFloat, style: FontStyle) -> UIFont {
        return UIFont(name: style.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func headerFont(style: FontStyle = .regular) -> UIFont {
        return regularFont(size: 18, style: style)
    }
}
