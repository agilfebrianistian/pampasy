//
//  HomeViewController.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 10/09/18.
//  Copyright © 2018 agil. All rights reserved.
//

import UIKit
import Floaty
import SwipeCellKit
import PopupController

class HomeViewController: UIViewController {
    
    var floaty = Floaty()
    
    @IBOutlet weak var topupButton: UIButton!
    @IBOutlet weak var transferButton: UIButton!
    @IBOutlet weak var pulsaButton: UIButton!
    @IBOutlet weak var plnButton: UIButton!
    
    @IBOutlet weak var segmentedTab: UISegmentedControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 160))
        imageView.contentMode = .scaleAspectFit
        let image = #imageLiteral(resourceName: "logo")
        imageView.image = image
        navigationItem.titleView = imageView
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(openPPOBMenu))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "avatar").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(openProfile))
        
        tableView.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
        tableView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        
        layoutFAB()
        setButtonView()
        
    }
    
    
    func setButtonView(){
        topupButton.alignTextBelow()
        transferButton.alignTextBelow()
        pulsaButton.alignTextBelow()
        plnButton.alignTextBelow()
    }
    
    @objc func openProfile() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController")
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }
    
    @objc func openPPOBMenu() {
        PopupController
            .create(self)
            .customize(
                [
                    .layout(.bottom),
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
                self.navigationItem.leftBarButtonItem?.isEnabled = false
            }
            .didCloseHandler { _ in
                print("closed popup!")
                self.navigationItem.leftBarButtonItem?.isEnabled = true
            }
            .show(PPOBPopup.instance())
    }
    
    func layoutFAB() {
        
        floaty.buttonImage = #imageLiteral(resourceName: "FloatingButton")
        floaty.hasShadow = true
        floaty.itemButtonColor = UIColor.init(red: 0/255, green: 128/255, blue: 187/255, alpha: 1)
        
        floaty.addItem("Undang Teman", icon:#imageLiteral(resourceName: "ic_undang")){ item in
            let alert = UIAlertController(title: "Test", message: "Undang Teman", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        floaty.addItem("Tarik Saldo", icon: #imageLiteral(resourceName: "ic_tarik"))
        floaty.addItem("Split Bills", icon: #imageLiteral(resourceName: "ic_split"))
        floaty.addItem("Request Saldo", icon: #imageLiteral(resourceName: "ic_request"))
        floaty.addItem("Transfer Saldo", icon: #imageLiteral(resourceName: "ic_transfer"))
        
        floaty.paddingX = self.view.frame.width/8 - floaty.frame.width/4
        floaty.fabDelegate = self
        
        self.view.addSubview(floaty)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func topupButtonTapped(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "TopupViewController")
        self.navigationController?.pushViewController(nextVC!, animated: true)
    }

    @IBAction func transferButtonTapped(_ sender: Any) {
    }

    @IBAction func pulsaButtonTapped(_ sender: Any) {
    }

    @IBAction func plnButtonTapped(_ sender: Any) {
    }

    
    
    @IBAction func notificationButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func postButtonTapped(_ sender: Any) {
        PopupController
            .create(self)
            .customize(
                [
                    .layout(.bottom),
                    .animation(.slideUp),
                    .scrollable(false),
                    .backgroundStyle(.blackFilter(alpha: 0.7))
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
                self.navigationItem.leftBarButtonItem?.isEnabled = false
            }
            .didCloseHandler { _ in
                print("closed popup!")
                self.navigationItem.leftBarButtonItem?.isEnabled = true
            }
            .show(PostPopup.instance())
    }
    
    @IBAction func tabTapped(_ sender: Any) {
        tableView.reloadData()
    }
    
}

extension HomeViewController: FloatyDelegate {
    
    // MARK: - Floaty Delegate Methods
    func floatyWillOpen(_ floaty: Floaty) {
        print("Floaty Will Open")
    }
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
    }
    
    func floatyWillClose(_ floaty: Floaty) {
        print("Floaty Will Close")
    }
    
    func floatyDidClose(_ floaty: Floaty) {
        print("Floaty Did Close")
    }
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "") { action, indexPath in
            // handle action by updating model with deletion
        }
        
        deleteAction.image = #imageLiteral(resourceName: "ic_exit")
        return [deleteAction]
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch segmentedTab.selectedSegmentIndex {
        case 1:
            return 64
        case 2 :
            return 74
        default:
            return 155
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segmentedTab.selectedSegmentIndex {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as! ChatCell
            return cell
        case 2 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell") as! SwipeTableViewCell
            cell.delegate = self
            return cell
        }
    }
}

