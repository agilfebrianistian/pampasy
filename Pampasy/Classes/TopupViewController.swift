//
//  TopupViewController.swift
//  Pampasy
//
//  Created by Agil Febrianistian on 08/10/18.
//  Copyright © 2018 agil. All rights reserved.
//

import UIKit

class TopupViewController: UIViewController {

    @IBOutlet weak var nominalButton: UIButton!
    @IBOutlet weak var pilihMetodeButton: UIButton!
    
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var totalView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nominalButton.layer.cornerRadius = nominalButton.frame.size.height/8
        nominalButton.layer.masksToBounds = true
        nominalButton.layer.borderColor = UIColor.lightGray.cgColor
        nominalButton.layer.borderWidth = 0.5
        
        detailView.layer.cornerRadius = detailView.frame.size.height/12
        detailView.layer.masksToBounds = true
        detailView.layer.borderColor = UIColor.lightGray.cgColor
        detailView.layer.borderWidth = 0.5

        totalView.layer.cornerRadius = totalView.frame.size.height/8
        totalView.layer.masksToBounds = true

        pilihMetodeButton.layer.cornerRadius = pilihMetodeButton.frame.size.height/8
        pilihMetodeButton.layer.masksToBounds = true

    }
}
